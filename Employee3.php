<?php




//Задача 9.1: Сделайте класс Employee, в котором будут следующие private свойства: name (имя), age (возраст) и salary (зарплата).
class Employee
{
    private $name;
    private $age;
    public $salary;
//Задача 9.2: Сделайте геттеры и сеттеры для всех свойств класса Employee.
    public function getName()
    {
        return $this->name;
    }
    public function getAge()
    {
        return $this->age;
    }
    //Задача 9.3: Дополните класс Employee приватным методом isAgeCorrect, который будет проверять возраст на корректность (от 1 до 100 лет).
    //Этот метод должен использоваться в сеттере setAge перед установкой нового возраста     (если возраст не корректный - он не должен меняться).
  private function isAgeCorrect($age){
        if (age > 1 or age < 100){
            $this ->age = $age;
        }
  }

//Задача 9.4: Пусть зарплата наших работников хранится в долларах. Сделайте так, чтобы геттер getSalary добавлял в конец числа с зарплатой значок доллара.
//То есть еще раз для полной ясности условия задачи:в свойстве salary зарплата будет хранится просто числом, но геттер будет возвращать ее с долларом на конце.
    public function getsSalary()
    {
        return $this->salary and '$';
    }

}