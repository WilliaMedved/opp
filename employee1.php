<?php

//Задача 2.1: Сделайте класс Employee (работник), в котором будут следующие свойства - name (имя), age (возраст), salary (зарплата).

class Employee
{
    public $name;
    public $age;
    public $salary;

//Задача 2.2: Сделайте в классе Employee метод getName, который будет возвращать имя работника.
    public function getName()
    {
        return $this->name;
    }

//Задача 2.3: Сделайте в классе Employee метод getAge, который будет возвращать возраст работника.
    public function getAge()
    {
        return $this->age;
    }
//Задача 2.5: Сделайте в классе Employee метод checkAge, который будет проверять то,
//что работнику больше 18 лет и возвращать true, если это так, и false, если это не так.
    public function checkAge($age): bool
    {
        if ($age >= 18) {
            return true;
        } else {
            return false;
        }
    }

//С помощью метода getSalary
    public function getSalary()
    {
        return $this->salary;
    }
}