<?php

class user
{
//Задача 7.1 (на приватные и публичные свойства и методы): смените модификатор доступа для вашего валидатора из пункта 6.5 в классе User.
//Теперь он должен быть не public, а private.
    private function valideAge($age){
        if(!isset ($age) or empty($age)){
            if(!is_string($age)){
                $this->age = $age;
            }
        }
    }
}